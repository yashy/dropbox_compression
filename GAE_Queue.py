#!/usr/bin/env python
"""
Author: Yash Yadav

"""


from flask import Flask
app = Flask(__name__)
app.config['DEBUG'] = True

import ConfigParser
import dropbox
import time

import threading
import boto.sqs
from boto.sqs.message import Message

import urllib
from pprint import pprint

from flask import Flask
from flask import request

import jinja2


from flask import Flask, request, abort
from pprint import *
from google.appengine.api import taskqueue

app = Flask(__name__)
app.config['DEBUG'] = True


"""
webhoooks: Response to the webhook
"""
@app.route('/webhook', methods=['GET'])
def verify():
    '''Respond to the webhook verification (GET request) by echoing back the challenge parameter.'''

    return request.args.get('challenge')


@app.route('/webhook', methods=['POST'])
def webhook():
    '''Receive a list of changed user IDs from Dropbox and process each.'''
    process_user()
    return ''

"""
Gets delta from the dropbox and processes files 
"""
def process_user():

    config = ConfigParser.RawConfigParser()
	config.read("settings.cfg")
	token = config.get('Dropbox', 'token')

	client = dropbox.client.DropboxClient(token)
	cursor = None
    
    #error check
    try:
    	delta_file = client.get_file('/.cursor')
    	cursor = delta_file.read()

    except:
    	print "Error File Does not exist"



	#if there is no content in file
	if cursor == '':
		cursor = None

	delta = client.delta(cursor)
	cursor = delta['cursor']    #Since delta is dictionary

	has_more=True
	while has_more:
		for path, metadata in delta['entries']:

            # Ignore deleted files, folders, and zip files
            if (metadata is None or
                    metadata['is_dir'] or
                    path.endswith('.gz')):
                continue
            else:
            	send_message(path)

		delta=client.delta(cursor)
		cursor=delta['cursor']	
		has_more=delta['has_more']

	client.put_file('/.cursor',cursor,overwrite=True)	

"""
Sending message to AWS Queue
"""
def send_message(path):
	config = ConfigParser.RawConfigParser()
	config.read("settings.cfg")
	access_key=config.get('AWS','aws_access_key_id')
	secret_key=config.get('AWS','aws_secret_access_key')
	conn = boto.sqs.connect_to_region("us-west-2",aws_access_key_id=aws_access_key_id,aws_secret_access_key=aws_secret_access_key)
	q = conn.create_queue('file', 10) # 10-second message visibility
	m=Message()
	m.set_body(path)
	q.write(m)







