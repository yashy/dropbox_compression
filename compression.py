#!/usr/bin/env python
"""
Author: Yash Yadav
"""
import urllib
from pprint import pprint

from flask import Flask
from flask import request

import jinja2
import gzip
import shutil
import sys

import threading
import boto.sqs
from boto.sqs.message import Message
import time

from flask import Flask
app = Flask(__name__)
app.config['DEBUG'] = True
import ConfigParser
import dropbox
import time




"""
Receives Message from an AWS SQS Queue 
Uses Boto and Runs in an EC2 Instance
"""
config = ConfigParser.RawConfigParser()
config.read("settings.cfg")
AWSid = config.get("AWS","aws_access_key_id")
AWSkey = config.get("AWS","aws_secret_access_key")
conn = boto.sqs.connect_to_region("us-west-2",aws_access_key_id=AWSid,aws_secret_access_key=AWSkey)
#check if queue exits
q = conn.create_queue('file', 10) # 10-second message visibility

while True:
	m = q.read(wait_time_seconds = 3) # wait up to 3 seconds for message
	if m is None:
		print "NO MESSAGE"

	else:
		message = m.get_body()        #stores message
		#file is read if it exists
		config = ConfigParser.RawConfigParser()
		config.read("settings.cfg")
		token = config.get('Dropbox', 'token')
		client = dropbox.client.DropboxClient(token)
		cursor = None

		file_name=client.get_file('/'+message)
		var = file_name.read()

		#File is compressed on the local machine
		with open(file_name, 'rb') as f_in:
			with gzip.open(file_name + '.gz', 'wb') as f_out:
				shutil.copyfileobj(f_in, f_out)
				# put_file method to put file on dropbox with error checking
				try:
					client.put_file(f_out, overwrite=True)
				except:
					print "Error: File not uploaded to dropbox"

		file_name.close()
		time.sleep(1)
		q.delete_message(m)




